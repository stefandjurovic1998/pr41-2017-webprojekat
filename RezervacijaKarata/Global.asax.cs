﻿using RezervacijaKarata.Models;
using RezervacijaKarata.Models.DB;
using RezervacijaKarataProjekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.SessionState;

namespace RezervacijaKarata
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            FileOperations fileReader = new FileOperations();

            MembersDB.sellersDB = fileReader.ReadSellersFile("sellers.txt");
            MembersDB.buyersDB = fileReader.ReadBuyersFile("buyers.txt");
            MembersDB.adminsDB = fileReader.ReadAdminsFile("admins.txt");

            EventDB.eventDB = fileReader.ReadEventFile("manifestacije.txt");
            TicketDB.ticketDB = fileReader.ReadTicketsFile("tickets.txt");
            CommentsDB.commentsDB = fileReader.ReadCommentsFile("comments.txt");
            
            foreach(var ticket in TicketDB.ticketDB)
            {
                foreach(var buyer in MembersDB.buyersDB)
                {
                    if (ticket.KupacKorisnickoIme.Equals(buyer.Username))
                    {
                        MembersDB.buyersDB[MembersDB.buyersDB.IndexOf(buyer)].ReservedTickets.Add(ticket.IdKarte);
                    }
                }
            }

            foreach (var _event in EventDB.eventDB)
            {
                foreach (var seller in MembersDB.sellersDB)
                {
                    if (_event.CreatedBy.Equals(seller.Username))
                    {
                        MembersDB.sellersDB[MembersDB.sellersDB.IndexOf(seller)].CreatedEvents.Add(_event);
                    }
                }
            }
        }

        public override void Init()
        {
            this.PostAuthenticateRequest += MvcApplication_PostAuthenticateRequest;
            base.Init();
        }

        void MvcApplication_PostAuthenticateRequest(object sender, EventArgs e)
        {
            System.Web.HttpContext.Current.SetSessionStateBehavior(
                SessionStateBehavior.Required);
        }
    }
}
