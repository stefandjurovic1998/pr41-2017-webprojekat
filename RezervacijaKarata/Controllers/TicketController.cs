﻿using RezervacijaKarata.Models;
using RezervacijaKarata.Models.CoreModels;
using RezervacijaKarata.Models.CoreModels.UserModels;
using RezervacijaKarataProjekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace RezervacijaKarata.Controllers
{
    public class TicketController : ApiController
    {
        
        [HttpGet]
        [Route("api/mytickets")]
        public IEnumerable<Karta> GetTickets()
        {
            var loggedInUser = (Buyer)HttpContext.Current.Session["Korisnik"];
            var foundMember = MembersDB.buyersDB.Find(mem => mem.Username.Equals(loggedInUser.Username));
            List<Karta> retList = new List<Karta>();
            foreach(var ticket in foundMember.ReservedTickets)
            {
                retList.Add(TicketDB.ticketDB.Find(tick => ((tick.IdKarte.Equals(ticket)) && (!tick.Deleted))));
            }
            return retList;
        }

        [HttpGet]
        [Route("api/sellertickets")]
        public IEnumerable<Karta> GetSellerTickets()
        {
            var loggedInUser = (Seller)HttpContext.Current.Session["Korisnik"];
            var foundMember = MembersDB.sellersDB.Find(mem => mem.Username.Equals(loggedInUser.Username));
            return TicketDB.ticketDB.FindAll(tick => tick.Manifestacija.CreatedBy.Equals(foundMember.Username));
        }

        [HttpPost]
        [Route("api/searchtickets")]
        public IEnumerable<Karta> SearchTickets([FromBody]SearchEvent rawTicket)
        {   
            var firstPartSearch = TicketDB.ticketDB.FindAll(tic => tic.Manifestacija.EventName.ToLower().Contains(rawTicket.EventName.ToLower()));
            var defaultMinDateValue = DateTime.MinValue;
            var defaultMaxDateValue = DateTime.MaxValue;
            var defaultMinPriceValue = int.MinValue;
            var defaultMaxPriceValue = int.MaxValue;

            if (!String.IsNullOrWhiteSpace(rawTicket.DateFrom))
                defaultMinDateValue = DateTime.Parse(rawTicket.DateFrom);
            if (!String.IsNullOrWhiteSpace(rawTicket.DateTo))
                defaultMaxDateValue = DateTime.Parse(rawTicket.DateTo);
            if (!String.IsNullOrWhiteSpace(rawTicket.PriceFrom))
                defaultMinPriceValue = int.Parse(rawTicket.PriceFrom);
            if (!String.IsNullOrWhiteSpace(rawTicket.PriceTo))
                defaultMaxPriceValue = int.Parse(rawTicket.PriceTo);


            List<Karta> returnList = new List<Karta>();
            foreach (var ticket in firstPartSearch)
            {
                if ((ticket.Cena >= defaultMinPriceValue) && (ticket.Cena <= defaultMaxPriceValue))
                    if (ticket.Datum >= defaultMinDateValue && ticket.Datum <= defaultMaxDateValue)
                        returnList.Add(ticket);
            }
            return returnList;
        }
        
        [HttpPost]
        [Route("api/unreservticket")]
        public HttpResponseMessage UnReserveTicket([FromBody]RawTicket rawTicket)
        {
            var foundTicket = TicketDB.ticketDB.Find(tick => tick.IdKarte.Equals(rawTicket.TicketId));
            var foundBuyer = MembersDB.buyersDB.Find(mem => mem.Username.Equals(foundTicket.KupacKorisnickoIme));

            foundBuyer.ReservationActivity.Add(DateTime.Now);

            var lastElements = foundBuyer.ReservationActivity;
            var lastElementsFromTheList = foundBuyer.ReservationActivity.Skip(Math.Max(0, foundBuyer.ReservationActivity.Count - 5));
            if (foundBuyer.ReservationActivity.Count >= 5)
            {
                var totalDays = (lastElementsFromTheList.First() - lastElementsFromTheList.Last()).TotalDays;
                if (totalDays < 30)
                    foundBuyer.Suspicious = true;
                else
                    foundBuyer.Suspicious = false;
            }

            var totalDaysBeforeAnEvent = (foundTicket.Manifestacija.EventDateTime - DateTime.Now).TotalDays;
            if (totalDaysBeforeAnEvent >= 7.0 && foundTicket.Status != StatusKarte.Odustanak)
            {
                int calculatePointsLoss = (int)CalculateRefundPoints((int)foundTicket.Cena);
                foundBuyer.Points -= calculatePointsLoss;
                foundBuyer.Membership = new Tip(foundBuyer.Points);

                foundTicket.Status = StatusKarte.Odustanak;

                FileOperations fileOperations = new FileOperations();

                fileOperations.SaveFile("tickets.txt", TicketDB.ticketDB);
                fileOperations.SaveFile("buyers.txt", MembersDB.buyersDB);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        private double CalculateRefundPoints(int ticket_price)
        {
            return ticket_price / 1000 * 133 * 4;
        }


        [HttpPost]
        [Route("api/deleteticket")]
        public HttpResponseMessage DeleteTicket([FromBody]RawTicket rawTicket)
        {
            var foundTicket = TicketDB.ticketDB.Find(tick => tick.IdKarte.Equals(rawTicket.TicketId));
            foundTicket.Deleted = true;
            FileOperations fileOperations = new FileOperations();
            fileOperations.SaveFile("tickets.txt", TicketDB.ticketDB);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
