﻿using RezervacijaKarata.Models;
using RezervacijaKarata.Models.CoreModels;
using RezervacijaKarata.Models.CoreModels.UserModels;
using RezervacijaKarataProjekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace RezervacijaKarata.Controllers
{
    public class AuthenticationController : ApiController
    {
        private FileOperations fileOperations = new FileOperations();

        [HttpPost]
        [Route("api/register")]
        public HttpResponseMessage Register([FromBody]AuthMember member)
        {
            Buyer foundMember = MembersDB.buyersDB.Find(mem => mem.Username.Equals(member.Username));
            if(foundMember == null)
            {
                //public Buyer(string firstName, string lastName, string birthDate, string gender, string username, string password, int points, bool deleted = false)
                Buyer newMember = new Buyer(member.FirstName, member.LastName, member.BirthDate, member.Gender, member.Username, member.Password, 0, false, false);
                newMember.Membership = new Tip(0);

                MembersDB.buyersDB.Add(newMember);
                fileOperations.SaveObject("buyers.txt", newMember.ToString());
                return Request.CreateResponse(HttpStatusCode.OK, newMember);
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        
        [HttpPost]
        [Route("api/registerseller")]
        public HttpResponseMessage RegisterSeller([FromBody]AuthMember member)
        {
            Seller foundMember = MembersDB.sellersDB.Find(mem => mem.Username.Equals(member.Username));
            if (foundMember == null)
            {
                Seller newMember = new Seller(member.FirstName, member.LastName, member.BirthDate, member.Gender, member.Username, member.Password, 0, false, false);
                newMember.Membership = new Tip(0);

                MembersDB.sellersDB.Add(newMember);
                fileOperations.SaveObject("sellers.txt", newMember.ToString());
                return Request.CreateResponse(HttpStatusCode.Accepted, newMember);
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        
        [HttpPost]
        [Route("api/login")]
        public HttpResponseMessage Login([FromBody]AuthMember member)
        {
            Seller foundSeller = MembersDB.sellersDB.Find(mem => mem.Username.Equals(member.Username));
            Buyer foundBuyer = MembersDB.buyersDB.Find(mem => mem.Username.Equals(member.Username));
            Admin foundAdmin = MembersDB.adminsDB.Find(mem => mem.Username.Equals(member.Username));

            if (foundSeller != null)
            {
                if(foundSeller.Password.Equals(member.Password) && !foundSeller.Blocked && !foundSeller.LogicallyDeleted)
                {
                    HttpContext.Current.Session["Korisnik"] = foundSeller;
                    return Request.CreateResponse(HttpStatusCode.Accepted, foundSeller);
                }
            }
            else if(foundBuyer != null)
            {
                if (foundBuyer.Password.Equals(member.Password) && !foundBuyer.Blocked && !foundBuyer.LogicallyDeleted)
                {
                    HttpContext.Current.Session["Korisnik"] = foundBuyer;
                    return Request.CreateResponse(HttpStatusCode.Accepted, (Member)foundBuyer);
                }
            }
            else if(foundAdmin != null)
            {
                if (foundAdmin.Password.Equals(member.Password))
                {
                    HttpContext.Current.Session["Korisnik"] = foundAdmin;
                    return Request.CreateResponse(HttpStatusCode.Accepted, foundAdmin);
                }
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        
        [HttpPost]
        [Route("api/loggedin")]
        public HttpResponseMessage PageAccessChecker()
        {
            if (HttpContext.Current.Session["Korisnik"] != null)
                return Request.CreateResponse(HttpStatusCode.Accepted);
            else
                return Request.CreateResponse(HttpStatusCode.Forbidden);
        }
       

        [HttpPost]
        [Route("api/logout")]
        public HttpResponseMessage LogOut()
        {
            if(HttpContext.Current.Session["Korisnik"] != null)
            {
                HttpContext.Current.Session["Korisnik"] = null;
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
        
    }
}
