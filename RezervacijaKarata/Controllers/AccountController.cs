﻿using RezervacijaKarata.Models;
using RezervacijaKarata.Models.CoreModels;
using RezervacijaKarata.Models.CoreModels.UserModels;
using RezervacijaKarata.Models.DB;
using RezervacijaKarataProjekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace RezervacijaKarata.Controllers
{
    public class AccountController : ApiController
    {
        
        [HttpGet]
        [Route("api/member")]
        public object GetMember()
        {
            return HttpContext.Current.Session["Korisnik"];
        }
        
        [HttpPost]
        [Route("api/updatemember")]
        public HttpResponseMessage UpdateMember([FromBody]Member updatedMember)
        {
            FileOperations fOperations = new FileOperations();
            var currentMember = new Member();

            if (MembersDB.buyersDB.Find(mem => mem.Username.Equals(updatedMember.Username)) != null)
            {
                currentMember = MembersDB.buyersDB.Find(mem => mem.Username.Equals(updatedMember.Username));
                currentMember.FirstName = updatedMember.FirstName;
                currentMember.LastName = updatedMember.LastName;
                currentMember.Gender = updatedMember.Gender;
                currentMember.BirthDate = updatedMember.BirthDate;
                currentMember.Password = updatedMember.Password;

                MembersDB.buyersDB[MembersDB.buyersDB.FindIndex(mem => mem.Username.Equals(updatedMember.Username))] = (Buyer)currentMember;
                HttpContext.Current.Session["Korisnik"] = currentMember;
                fOperations.SaveFile("buyers.txt", MembersDB.buyersDB);
                return Request.CreateResponse(HttpStatusCode.Accepted, updatedMember);
            }
            else if(MembersDB.sellersDB.Find(mem => mem.Username.Equals(updatedMember.Username)) != null)
            {
                currentMember = MembersDB.sellersDB.Find(mem => mem.Username.Equals(updatedMember.Username));
                currentMember.FirstName = updatedMember.FirstName;
                currentMember.LastName = updatedMember.LastName;
                currentMember.Gender = updatedMember.Gender;
                currentMember.BirthDate = updatedMember.BirthDate;
                currentMember.Password = updatedMember.Password;

                MembersDB.sellersDB[MembersDB.sellersDB.FindIndex(mem => mem.Username.Equals(updatedMember.Username))] = (Seller)currentMember;
                HttpContext.Current.Session["Korisnik"] = currentMember;
                fOperations.SaveFile("sellers.txt", MembersDB.sellersDB);
                return Request.CreateResponse(HttpStatusCode.Accepted, updatedMember);
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        
        [HttpGet]
        [Route("api/getallmembers")]
        public List<Member> ReturnAllMembers()
        {
            var returnList = new List<Member>();
            foreach (var buyer in MembersDB.buyersDB)
            {
                if (!buyer.LogicallyDeleted)
                    returnList.Add((Member)buyer);
            }
            foreach (var seller in MembersDB.sellersDB)
            {
                if(!seller.LogicallyDeleted)
                    returnList.Add((Member)seller);
            }
            return returnList;
        }

        [HttpGet]
        [Route("api/getbuyers")]
        public IEnumerable<Member> ReturnBuyers()
        {
            return MembersDB.buyersDB.FindAll(mem => ((mem.LogicallyDeleted.Equals(false))));
        }

        [HttpGet]
        [Route("api/getseller")]
        public IEnumerable<Member> ReturnSellers()
        {
            return MembersDB.sellersDB.FindAll(mem => ((mem.LogicallyDeleted.Equals(false))));
        }

        [HttpPost]
        [Route("api/searchmembers")]
        public IEnumerable<Member> SearchMembers([FromBody]AuthMember searchMember)
        {
            var firstname = searchMember.FirstName;
            var lastname = searchMember.LastName;
            var username = searchMember.Username;

            if (firstname == null)
                firstname = "";
            else if (lastname == null)
                lastname = "";
            else if (username == null)
                username = "";

            List<Member> retList = new List<Member>();
            var sellers = MembersDB.sellersDB.FindAll(mem => ((mem.FirstName.ToLower().Contains(firstname.ToLower())) && (mem.LastName.ToLower().Contains(lastname.ToLower())) && (mem.Username.ToLower().Contains(username.ToLower()))));
            var buyers = MembersDB.buyersDB.FindAll(mem => ((mem.FirstName.ToLower().Contains(firstname.ToLower())) && (mem.LastName.ToLower().Contains(lastname.ToLower())) && (mem.Username.ToLower().Contains(username.ToLower()))));
            foreach (var mem in sellers)
                retList.Add(mem);
            foreach (var mem in buyers)
                retList.Add(mem);
            return retList;

        }

        [HttpPost]
        [Route("api/removemember")]
        public HttpResponseMessage DeleteMember([FromBody]AuthMember authMember)
        {
            var foundMember = new Member();
            if (authMember.MemberType == "Buyer")
                foundMember = MembersDB.buyersDB.Find(mem => mem.Username.Equals(authMember.Username));
            else
                foundMember = MembersDB.sellersDB.Find(mem => mem.Username.Equals(authMember.Username));

            if (foundMember != null)
            {
                FileOperations fileOperations = new FileOperations();
                if (authMember.MemberType == "Buyer")
                {
                    // DELETE ALL OF USERS TICKETS & COMMENTS
                    foreach (var comment in CommentsDB.commentsDB)
                    {
                        if (comment.Kupac.Username.Equals(authMember.Username))
                            comment.Deleted = true;
                    }

                    foreach (var ticket in TicketDB.ticketDB)
                    {
                        if (ticket.KupacKorisnickoIme.Equals(authMember.Username))
                            ticket.Deleted = true;
                    }

                    MembersDB.buyersDB[MembersDB.buyersDB.IndexOf((Buyer)foundMember)].LogicallyDeleted = true;

                    fileOperations.SaveFile("buyers.txt", MembersDB.buyersDB);
                    fileOperations.SaveFile("comments.txt", CommentsDB.commentsDB);
                    fileOperations.SaveFile("tickets.txt", TicketDB.ticketDB);
                }
                else
                {

                    // DELETE ALL OF EVENTS & THE RESERVED TICKETS FOR THAT EVENT
                    foreach(var _event in EventDB.eventDB)
                    {
                        if(_event.CreatedBy.Equals(foundMember.Username))
                        {
                            foreach(var ticket in TicketDB.ticketDB)
                            {
                                if (ticket.Manifestacija.Equals(_event))
                                    ticket.Deleted = true;
                            }

                            foreach(var comment in CommentsDB.commentsDB)
                            {
                                if (comment.Manifestacija.Equals(_event))
                                    comment.Deleted = true;
                            }

                            _event.LogicallyDeleted = true;
                        }

                        MembersDB.sellersDB[MembersDB.sellersDB.IndexOf((Seller)foundMember)].LogicallyDeleted = true;

                        fileOperations.SaveFile("sellers.txt", MembersDB.sellersDB);
                        fileOperations.SaveFile("manifestacije.txt", EventDB.eventDB);
                        fileOperations.SaveFile("comments.txt", CommentsDB.commentsDB);
                        fileOperations.SaveFile("tickets.txt", TicketDB.ticketDB);
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route("api/blockmember")]
        public HttpResponseMessage BlockMember ([FromBody]AuthMember authMember)
        {   
            FileOperations fileOperations = new FileOperations();
            if (authMember.MemberType == "Buyer")
            {
                var foundMember = MembersDB.buyersDB.Find(mem => mem.Username.Equals(authMember.Username));
                if (foundMember != null)
                {
                    foundMember.Blocked = true;
                    
                    fileOperations.SaveFile("buyers.txt", MembersDB.buyersDB);
                    return Request.CreateResponse(HttpStatusCode.OK);
                    
                }
                else
                    return Request.CreateResponse(HttpStatusCode.Conflict);
            }
            else
            {
                var foundMember = MembersDB.sellersDB.Find(mem => mem.Username.Equals(authMember.Username));
                if (foundMember != null)
                {
                    foundMember.Blocked = true;
                    fileOperations.SaveFile("sellers.txt", MembersDB.sellersDB);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.Conflict);
            }
        }

        [HttpPost]
        [Route("api/unblockmember")]
        public HttpResponseMessage UnblockMember ([FromBody]AuthMember authMember)
        {
            FileOperations fileOperations = new FileOperations();
            if (authMember.MemberType == "Buyer")
            {
                var foundMember = MembersDB.buyersDB.Find(mem => mem.Username.Equals(authMember.Username));
                if (foundMember != null)
                {
                    foundMember.Blocked = false;
                    fileOperations.SaveFile("buyers.txt", MembersDB.buyersDB);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.Conflict);
            }
            else
            {
                var foundMember = MembersDB.sellersDB.Find(mem => mem.Username.Equals(authMember.Username));
                if (foundMember != null)
                {
                    foundMember.Blocked = false;
                    fileOperations.SaveFile("sellers.txt", MembersDB.sellersDB);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.Conflict);
            }
        }
    }
}
