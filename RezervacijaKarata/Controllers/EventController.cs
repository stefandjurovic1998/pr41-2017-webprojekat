﻿using RezervacijaKarata.Models;
using RezervacijaKarata.Models.CoreModels;
using RezervacijaKarata.Models.CoreModels.UserModels;
using RezervacijaKarata.Models.DB;
using RezervacijaKarata.Models.RawModels;
using RezervacijaKarataProjekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace RezervacijaKarata.Controllers
{
    public class EventController : ApiController
    {
        // creating string for ticket id 
        private static Random random = new Random();
        public static string RandomString()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var generatedString = "";
            bool uniqueID = false;
            while (uniqueID == false)
            {
                generatedString = new string(Enumerable.Repeat(chars, 10).Select(s => s[random.Next(s.Length)]).ToArray());
                var checkId = TicketDB.ticketDB.Find(tick => tick.IdKarte.Equals(generatedString));
                if (checkId == null)
                    break;
            }
            return generatedString;
        }

        [HttpPost]
        [Route("api/eventregistration")]
        public HttpResponseMessage EventRegistration([FromBody]RawEvent rawEvent)
        {
            var foundEvent = EventDB.eventDB.Find(evnt => (evnt.EventLocation.UlicaIBroj.Equals(rawEvent.EventStreetAdress)) &&
                                                          (evnt.EventLocation.Grad.Equals(rawEvent.EventCity)) && (evnt.EventLocation.PostanskiBroj.Equals(rawEvent.EventPostalCode)) &&
                                                          (evnt.EventDateTime.Equals(DateTime.Parse(rawEvent.EventDate))));
            if (foundEvent == null)
            {
                var eventTypeEnum = (TipManifestacija)Enum.Parse(typeof(TipManifestacija), rawEvent.EventType);
                var eventDateTime = DateTime.Parse(rawEvent.EventDate);
                var eventAttendence = int.Parse(rawEvent.RegularTickets)+int.Parse(rawEvent.FanPitTickets)+int.Parse(rawEvent.VipTickets);
                var eventPriceTicket = int.Parse(rawEvent.TicketPrice);

                var foundMember = (Seller)HttpContext.Current.Session["Korisnik"];

                Mesto mesto = new Mesto(rawEvent.EventStreetAdress, rawEvent.EventCity, rawEvent.EventPostalCode, rawEvent.EventLatitude, rawEvent.EventLongitude);
                Manifestacija manifestacija = new Manifestacija(rawEvent.EventName, foundMember.Username, eventTypeEnum, eventAttendence, 0,0,0, eventDateTime, eventPriceTicket, mesto, rawEvent.EventPoster, false, false);

                manifestacija.TicketType = new List<Tuple<string, int>>(3)
                {
                    Tuple.Create("Regular", int.Parse(rawEvent.RegularTickets)),
                    Tuple.Create("Vip", int.Parse(rawEvent.VipTickets)),
                    Tuple.Create("FanPit", int.Parse(rawEvent.FanPitTickets)),
                };

                manifestacija.LogicallyDeleted = false;
                manifestacija.ActivePost = false;

                var sellerAccount = (Seller)HttpContext.Current.Session["Korisnik"];
                sellerAccount.CreatedEvents.Add(manifestacija);

                EventDB.eventDB.Add(manifestacija);

                FileOperations fOperations = new FileOperations();
                fOperations.SaveObject("manifestacije.txt", manifestacija.ToString());
                return Request.CreateResponse(HttpStatusCode.OK);
            }

            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpGet]
        [Route("api/getinactiveevents")]
        public IEnumerable<Manifestacija> GetInactiveEvents()
        {
            return EventDB.eventDB.FindAll(evnt => ((evnt.ActivePost.Equals(false)) && (evnt.LogicallyDeleted.Equals(false))));
        }

        [HttpGet]
        [Route("api/getactiveevents")]
        public IEnumerable<Manifestacija> GetActiveEvents()
        {
            var allEvents = EventDB.eventDB.FindAll(evnt => ((evnt.ActivePost.Equals(true)) && (evnt.LogicallyDeleted.Equals(false))));
            return allEvents.OrderBy(evnt => evnt.EventDateTime).ToList();
        }

        [HttpGet]
        [Route("api/getallevents")]
        public IEnumerable<Manifestacija> GetAllEvents()
        {
            return EventDB.eventDB.FindAll(evnt => (evnt.LogicallyDeleted.Equals(false)));
        }

        [HttpPost]
        [Route("api/activeevent")]
        public HttpResponseMessage ActivateEvent([FromBody]RawEvent rawEvent)
        {
            var foundEvent = EventDB.eventDB.Find(evnt => (evnt.EventLocation.UlicaIBroj.Equals(rawEvent.EventStreetAdress)) &&
                                                          (evnt.EventLocation.Grad.Equals(rawEvent.EventCity)) && (evnt.EventLocation.PostanskiBroj.Equals(rawEvent.EventPostalCode)) &&
                                                          (evnt.EventDateTime.Equals(DateTime.Parse(rawEvent.EventDate))));
            if (foundEvent != null)
            {
                EventDB.eventDB[EventDB.eventDB.IndexOf(foundEvent)].ActivePost = true;
                FileOperations fileOperations = new FileOperations();
                fileOperations.SaveFile("manifestacije.txt", EventDB.eventDB);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpPost]
        [Route("api/searchevents")]
        public IEnumerable<Manifestacija> SearchEvent([FromBody]SearchEvent rawEvent)
        {
            //var activeEvents = EventDB.eventDB.FindAll(evnt => evnt.ActivePost.Equals(true));
            var firstPartSearch = EventDB.eventDB.FindAll(evnt => ((evnt.EventLocation.UlicaIBroj.ToLower().Contains(rawEvent.EventLocation.ToLower())) &&
                                                          (evnt.EventLocation.Grad.ToLower().Contains(rawEvent.EventCity.ToLower())) &&
                                                          (evnt.EventName.ToLower().Contains(rawEvent.EventName.ToLower()))
                                                ));

            var defaultMinDateValue = DateTime.MinValue;
            var defaultMaxDateValue = DateTime.MaxValue;
            var defaultMinPriceValue = int.MinValue;
            var defaultMaxPriceValue = int.MaxValue;

            if (!String.IsNullOrWhiteSpace(rawEvent.DateFrom))
                defaultMinDateValue = DateTime.Parse(rawEvent.DateFrom);
            if (!String.IsNullOrWhiteSpace(rawEvent.DateTo))
                defaultMaxDateValue = DateTime.Parse(rawEvent.DateTo);
            if (!String.IsNullOrWhiteSpace(rawEvent.PriceFrom))
                defaultMinPriceValue = int.Parse(rawEvent.PriceFrom);
            if (!String.IsNullOrWhiteSpace(rawEvent.PriceTo))
                defaultMaxPriceValue = int.Parse(rawEvent.PriceTo);


            List<Manifestacija> returnList = new List<Manifestacija>();
            foreach (var evnt in firstPartSearch)
            {
                if ((evnt.TicketPrice >= defaultMinPriceValue) && (evnt.TicketPrice <= defaultMaxPriceValue))
                    if (evnt.EventDateTime.Date >= defaultMinDateValue && evnt.EventDateTime.Date <= defaultMaxDateValue)
                        returnList.Add(evnt);
            }
            return returnList;
        }

        [HttpPost]
        [Route("api/getanevent")]
        public Manifestacija GetEvent([FromBody]RawEvent rawEvent)
        {
            return EventDB.eventDB.Find(evnt => (evnt.EventLocation.UlicaIBroj.Equals(rawEvent.EventStreetAdress)) &&
                                                           (evnt.EventLocation.Grad.Equals(rawEvent.EventCity)) && (evnt.EventLocation.PostanskiBroj.Equals(rawEvent.EventPostalCode)) &&
                                                           (evnt.EventDateTime.Equals(DateTime.Parse(rawEvent.EventDate))));
        }

        [HttpPost]
        [Route("api/deleteevent")]
        public HttpResponseMessage DeleteEvent([FromBody]RawEvent rawEvent)
        {
            var foundEvent = EventDB.eventDB.Find(evnt => (evnt.EventLocation.UlicaIBroj.Equals(rawEvent.EventStreetAdress)) &&
                                                          (evnt.EventLocation.Grad.Equals(rawEvent.EventCity)) && (evnt.EventLocation.PostanskiBroj.Equals(rawEvent.EventPostalCode) &&
                                                          (evnt.EventDateTime.Equals(DateTime.Parse(rawEvent.EventDate)))));
            if (foundEvent != null)
            {
                var foundCreator = MembersDB.sellersDB.Find(mem => mem.Username.Equals(rawEvent.CreatedBy));
                foundCreator.CreatedEvents[foundCreator.CreatedEvents.IndexOf(foundEvent)].LogicallyDeleted = true;

                // tickets
                foreach(var ticket in TicketDB.ticketDB)
                {
                    if (ticket.Manifestacija.Equals(foundEvent))
                        ticket.Deleted = true;
                }

                foreach(var comment in CommentsDB.commentsDB)
                {
                    if (comment.Manifestacija.Equals(foundEvent))
                        comment.Deleted = true;
                }

                foundEvent.LogicallyDeleted = true;
                FileOperations fileOperations = new FileOperations();

                //EventDB.eventDB[EventDB.eventDB.IndexOf(foundEvent)] = foundEvent;
                foundEvent.LogicallyDeleted = true;
                fileOperations.SaveFile("manifestacije.txt", EventDB.eventDB);
                fileOperations.SaveFile("comments.txt", CommentsDB.commentsDB);
                fileOperations.SaveFile("tickets.txt", TicketDB.ticketDB);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpPost]
        [Route("api/reservetickets")]
        public HttpResponseMessage ReserveTicket([FromBody]RawEvent rawEvent)
        {
            var foundEvent = EventDB.eventDB.Find(evnt => (evnt.EventLocation.UlicaIBroj.Equals(rawEvent.EventStreetAdress)) &&
                                                          (evnt.EventLocation.Grad.Equals(rawEvent.EventCity)) && (evnt.EventLocation.PostanskiBroj.Equals(rawEvent.EventPostalCode)) &&
                                                          (evnt.EventDateTime.Equals(DateTime.Parse(rawEvent.EventDate))));
            var foundMember = (Buyer)HttpContext.Current.Session["Korisnik"];
            int points_gathered = 0;

            if (foundEvent != null && foundMember != null && foundEvent.EventDateTime > DateTime.Now)
            {
                FileOperations fileOperations = new FileOperations();

                int leftRegularTickets = (foundEvent.TicketType[0].Item2 - foundEvent.RegularTicketsSold);
                int leftFanPitTickets = (foundEvent.TicketType[1].Item2 - foundEvent.FanPitTicketsSold);
                int leftVipTickets = (foundEvent.TicketType[2].Item2 - foundEvent.VipTicketsSold);

                if (rawEvent.RegularTickets != "")
                {
                    if (leftRegularTickets < int.Parse(rawEvent.RegularTickets))
                        return Request.CreateResponse(HttpStatusCode.BadRequest, rawEvent);
                    else
                    {
                        for (int i = 0; i < int.Parse(rawEvent.RegularTickets); i++)
                        {
                            int discountedPrice = (int)(foundEvent.TicketPrice * foundMember.Membership.Popust);
                            points_gathered += CheckoutPointCalculations(discountedPrice);

                            Karta newTicket = new Karta(RandomString(), foundEvent, foundEvent.EventDateTime, discountedPrice, foundMember.FirstName, foundMember.LastName, foundMember.Username, StatusKarte.Rezervisana, TipKarte.REGULAR, false);
                            foundMember.ReservedTickets.Add(newTicket.IdKarte);

                            TicketDB.ticketDB.Add(newTicket);
                            fileOperations.SaveObject("tickets.txt", newTicket.ToString());

                            foundEvent.RegularTicketsSold++;
                            foundEvent.TicketsSold++;
                        }
                    }
                }

                if (rawEvent.FanPitTickets != "")
                {
                    if (leftFanPitTickets < int.Parse(rawEvent.FanPitTickets))
                        return Request.CreateResponse(HttpStatusCode.BadRequest);
                    else
                    {
                        for (int i = 0; i < int.Parse(rawEvent.FanPitTickets); i++)
                        {
                            int discountedPrice = (int)(foundEvent.TicketPrice * 2 * foundMember.Membership.Popust);
                            points_gathered += CheckoutPointCalculations(discountedPrice);

                            Karta newTicket = new Karta(RandomString(), foundEvent, DateTime.Now, discountedPrice, foundMember.FirstName, foundMember.LastName, foundMember.Username, StatusKarte.Rezervisana, TipKarte.FAN_PIT, false);
                            foundMember.ReservedTickets.Add(newTicket.IdKarte);

                            TicketDB.ticketDB.Add(newTicket);
                            fileOperations.SaveObject("tickets.txt", newTicket.ToString());

                            foundEvent.FanPitTicketsSold++;
                            foundEvent.TicketsSold++;
                        }
                    }
                }

                if(rawEvent.VipTickets != "")
                {
                    if (leftVipTickets < int.Parse(rawEvent.VipTickets))
                        return Request.CreateResponse(HttpStatusCode.BadRequest);
                    else
                    {
                        for (int i = 0; i < int.Parse(rawEvent.VipTickets); i++)
                        {
                            int discountedPrice = (int)(foundEvent.TicketPrice * 4 * foundMember.Membership.Popust);
                            points_gathered += CheckoutPointCalculations(discountedPrice);

                            Karta newTicket = new Karta(RandomString(), foundEvent, DateTime.Now, discountedPrice, foundMember.FirstName, foundMember.LastName, foundMember.Username, StatusKarte.Rezervisana, TipKarte.VIP, false);
                            foundMember.ReservedTickets.Add(newTicket.IdKarte);

                            TicketDB.ticketDB.Add(newTicket);
                            fileOperations.SaveObject("tickets.txt", newTicket.ToString());

                            foundEvent.VipTicketsSold++;
                            foundEvent.TicketsSold++;
                        }
                    }
                }

                MembersDB.buyersDB[MembersDB.buyersDB.IndexOf(foundMember)].Points += points_gathered;
                MembersDB.buyersDB[MembersDB.buyersDB.IndexOf(foundMember)].Membership = new Tip(foundMember.Points);
                var foundSeller = MembersDB.sellersDB.Find(mem => mem.Username.Equals(foundEvent.CreatedBy));
                foundSeller.CreatedEvents[foundSeller.CreatedEvents.IndexOf(foundEvent)] = foundEvent;
                
                //foundMember.Points += points_gathered;
                //foundMember.Membership = new Tip(foundMember.Points);
                fileOperations.SaveFile("buyers.txt", MembersDB.buyersDB);
                fileOperations.SaveFile("manifestacije.txt", EventDB.eventDB);
                return Request.CreateResponse(HttpStatusCode.OK, rawEvent);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, rawEvent);
        }

        private int CheckoutPointCalculations(int ticket_price)
        {
            return ticket_price / 1000 * 133;
        }

        [HttpGet]
        [Route("api/userevents")]
        public IEnumerable<Manifestacija> ReturnEvents()
        {
            var loggedInUser = (Seller)HttpContext.Current.Session["Korisnik"];
            var foundMember = MembersDB.sellersDB.Find(mem => mem.Username.Equals(loggedInUser.Username));
            List<Manifestacija> retList = new List<Manifestacija>();
            return foundMember.CreatedEvents;
        }

        [HttpPost]
        [Route("api/updatevent")]
        public HttpResponseMessage UpdateEvent([FromBody]RawEvent rawEvent)
        {
            var foundEvent = EventDB.eventDB.Find(evnt => (evnt.EventLocation.UlicaIBroj.Equals(rawEvent.CurrentLocation)) &&
                                                          (evnt.EventLocation.Grad.Equals(rawEvent.CurrentCity)) && (evnt.EventLocation.PostanskiBroj.Equals(rawEvent.CurrentPostalCode)) &&
                                                          (evnt.EventDateTime.Equals(DateTime.Parse(rawEvent.CurrentDate))));
            if (foundEvent != null)
            {
                var eventTypeEnum = (TipManifestacija)Enum.Parse(typeof(TipManifestacija), rawEvent.EventType);
                var eventDateTime = DateTime.Parse(rawEvent.EventDate);
                var eventAttendence = int.Parse(rawEvent.RegularTickets) +int.Parse(rawEvent.FanPitTickets) +int.Parse(rawEvent.VipTickets);
                var eventPriceTicket = int.Parse(rawEvent.TicketPrice);

                var foundMember = (Seller)HttpContext.Current.Session["Korisnik"];

                Mesto mesto = new Mesto(rawEvent.EventStreetAdress, rawEvent.EventCity, rawEvent.EventPostalCode, rawEvent.EventLatitude, rawEvent.EventLongitude);
                // event on start isn't logiclly deleted, but it's still isn't activate, false false 
                Manifestacija manifestacija = new Manifestacija(rawEvent.EventName, foundMember.Username, eventTypeEnum, eventAttendence, foundEvent.RegularTicketsSold, foundEvent.FanPitTicketsSold, foundEvent.VipTicketsSold, eventDateTime, eventPriceTicket, mesto, rawEvent.EventPoster, false, false);

                manifestacija.TicketType = new List<Tuple<string, int>>(3)
                {
                    Tuple.Create("Regular", int.Parse(rawEvent.RegularTickets)),
                    Tuple.Create("Vip", int.Parse(rawEvent.VipTickets)),
                    Tuple.Create("FanPit", int.Parse(rawEvent.FanPitTickets)),
                };

                manifestacija.LogicallyDeleted = foundEvent.LogicallyDeleted;
                manifestacija.ActivePost = foundEvent.ActivePost;

                EventDB.eventDB[EventDB.eventDB.IndexOf(foundEvent)] = manifestacija;
                var loggedInSeller = (Seller)HttpContext.Current.Session["Korisnik"];
                loggedInSeller.CreatedEvents[loggedInSeller.CreatedEvents.IndexOf(foundEvent)] = manifestacija;
                
                foreach(var ticket in TicketDB.ticketDB)
                {
                    if (ticket.Manifestacija.Equals(foundEvent))
                        ticket.Manifestacija = manifestacija;
                }

                /*
                foreach (var _event in loggedInSeller.CreatedEvents)
                {
                    if (_event.EventDateTime.Equals(DateTime.Parse(rawEvent.CurrentDate)))
                    {
                        loggedInSeller.CreatedEvents[loggedInSeller.CreatedEvents.IndexOf(_event)] = manifestacija;
                    }
                }
                */
                foundEvent = manifestacija;
                MembersDB.sellersDB[MembersDB.sellersDB.IndexOf(MembersDB.sellersDB.Find(mem => mem.Username.Equals(loggedInSeller.Username)))] = loggedInSeller;
                FileOperations fOperations = new FileOperations();
                fOperations.SaveFile("manifestacije.txt", EventDB.eventDB);
                fOperations.SaveFile("tickets.txt", TicketDB.ticketDB);
                return Request.CreateResponse(HttpStatusCode.OK, rawEvent);
            }
            return Request.CreateResponse(HttpStatusCode.Conflict, rawEvent);
        }

        [HttpPost]
        [Route("api/postcomment")]
        public HttpResponseMessage CreateComment([FromBody]RawComment rawComment)
        {
            var foundEvent = EventDB.eventDB.Find(evnt => (evnt.EventLocation.UlicaIBroj.Equals(rawComment.EventStreetAdress)) &&
                                                          (evnt.EventLocation.Grad.Equals(rawComment.EventCity)) && (evnt.EventLocation.PostanskiBroj.Equals(rawComment.EventPostalCode) &&
                                                          (evnt.EventDateTime.Equals(DateTime.Parse(rawComment.EventDate)))));
            var foundMember = (Buyer)HttpContext.Current.Session["Korisnik"];

            var ocenaKorisnika = "";
            switch (rawComment.CommentRating)
            {
                case "0":
                    ocenaKorisnika = "Nula";
                    break;
                case "1":
                    ocenaKorisnika = "Jedan";
                    break;
                case "2":
                    ocenaKorisnika = "Dva";
                    break;
                case "3":
                    ocenaKorisnika = "Tri";
                    break;
                case "4":
                    ocenaKorisnika = "Cetiri";
                    break;
                case "5":
                    ocenaKorisnika = "Pet";
                    break;
            }

            bool userHadReservedTicket = false;
            foreach (var ticket in foundMember.ReservedTickets)
            {
                var reservedTicket = TicketDB.ticketDB.Find(tick => (tick.IdKarte.Equals(ticket) && tick.Manifestacija.Equals(foundEvent)));
                if((reservedTicket != null) && (reservedTicket.Status==StatusKarte.Rezervisana))
                {
                    userHadReservedTicket = true;
                    break;
                }
            }

            if (foundEvent != null && foundMember != null)
            {
                if (foundEvent.EventDateTime <= DateTime.Now && userHadReservedTicket)
                {
                    FileOperations fileOperations = new FileOperations();

                    Komentar newKomentar = new Komentar(foundMember, foundEvent, DateTime.Now, rawComment.CommentText, (Ocena)Enum.Parse(typeof(Ocena), ocenaKorisnika), false, false);

                    CommentsDB.commentsDB.Add(newKomentar);
                    fileOperations.SaveObject("comments.txt", newKomentar.ToString());
                    return Request.CreateResponse(HttpStatusCode.OK, newKomentar);
                }
            }
            return Request.CreateResponse(HttpStatusCode.Conflict, rawComment);
        }

        [HttpPost]
        [Route("api/getcomments")]
        public IEnumerable<Komentar> ReturnEventCommnets([FromBody]RawEvent rawEvent)
        {
            var foundEvent = EventDB.eventDB.Find(evnt => (evnt.EventLocation.UlicaIBroj.Equals(rawEvent.EventStreetAdress)) &&
                                                          (evnt.EventLocation.Grad.Equals(rawEvent.EventCity)) && (evnt.EventLocation.PostanskiBroj.Equals(rawEvent.EventPostalCode) &&
                                                          (evnt.EventDateTime.Equals(DateTime.Parse(rawEvent.EventDate)))));
            List<Komentar> returnList = new List<Komentar>();
            foreach(var comment in CommentsDB.commentsDB)
            {
                if (comment.Manifestacija.Equals(foundEvent) && (comment.Approved==true) && (comment.Deleted == false))
                    returnList.Add(comment);
            }
            return returnList;
        }

        [HttpGet]
        [Route("api/getsellercomments")]
        public IEnumerable<Komentar> ReturnSellerComments()
        {
            var loggedSeller = (Seller)HttpContext.Current.Session["Korisnik"];
            return CommentsDB.commentsDB.FindAll(com => com.Manifestacija.CreatedBy.Equals(loggedSeller.Username));
        }

        [HttpGet]
        [Route("api/getallcomments")]
        public IEnumerable<Komentar> ReturnAllComments()
        {
            return CommentsDB.commentsDB;
        }

        [HttpPost]
        [Route("api/approvecomment")]
        public HttpResponseMessage ApproveComment([FromBody]RawComment rawComment)
        {
            var parsedRating = "";
            switch(rawComment.CommentRating)
            {
                case "0":
                    parsedRating = "Nula";
                    break;
                case "1":
                    parsedRating = "Jedan";
                    break;
                case "2":
                    parsedRating = "Dva";
                    break;
                case "3":
                    parsedRating = "Tri";
                    break;
                case "4":
                    parsedRating = "Cetiri";
                    break;
                case "5":
                    parsedRating = "Pet";
                    break;
            }
            var parsedRatingEnum = (Ocena)Enum.Parse(typeof(Ocena), parsedRating);
            var foundComment = CommentsDB.commentsDB.Find(comm => (comm.Kupac.Username.Equals(rawComment.PostedBy) && comm.Posted.Equals(DateTime.Parse(rawComment.PostedWhen))
                                                            && comm.Tekst.Contains(rawComment.CommentText)) && comm.Ocena.Equals(parsedRatingEnum));
           
            foundComment.Approved = true;
            FileOperations fileOperations = new FileOperations();
            fileOperations.SaveFile("comments.txt", CommentsDB.commentsDB);
            return Request.CreateResponse(HttpStatusCode.OK);
        }   

        [HttpPost]
        [Route("api/deletecomment")]
        public HttpResponseMessage DeleteComment([FromBody]RawComment rawComment)
        {
            var parsedRating = "";
            switch (rawComment.CommentRating)
            {
                case "0":
                    parsedRating = "Nula";
                    break;
                case "1":
                    parsedRating = "Jedan";
                    break;
                case "2":
                    parsedRating = "Dva";
                    break;
                case "3":
                    parsedRating = "Tri";
                    break;
                case "4":
                    parsedRating = "Cetiri";
                    break;
                case "5":
                    parsedRating = "Pet";
                    break;
            }
            var parsedRatingEnum = (Ocena)Enum.Parse(typeof(Ocena), parsedRating);
            var foundComment = CommentsDB.commentsDB.Find(comm => (comm.Kupac.Username.Equals(rawComment.PostedBy) && comm.Posted.Equals(DateTime.Parse(rawComment.PostedWhen))
                                                            && comm.Tekst.Contains(rawComment.CommentText)) && comm.Ocena.Equals(parsedRatingEnum));

            foundComment.Deleted = true;
            FileOperations fileOperations = new FileOperations();
            fileOperations.SaveFile("comments.txt", CommentsDB.commentsDB);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
