﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaKarataProjekat.Models
{
    public enum StatusKarte {Rezervisana, Odustanak}
    public enum TipKarte { REGULAR, FAN_PIT, VIP}

    public class Karta
    {
        public String IdKarte { get; set; }
        public Manifestacija Manifestacija { get; set; }
        public DateTime Datum { get; set; }
        public double Cena { get; set; }
        public String KupacIme { get; set; }
        public String KupacPrezime { get; set; }
        public String KupacKorisnickoIme { get; set; }
        public StatusKarte Status { get; set; }
        public TipKarte Tip { get; set; }
        public bool Deleted { get; set; }

        public Karta(string idKarte, Manifestacija manifestacija, DateTime datum, double cena, string ime, string prezime, string korisnickoIme, StatusKarte status, TipKarte tip, bool deleted)
        {
            IdKarte = idKarte;
            Manifestacija = manifestacija;
            Datum = datum;
            Cena = cena;

            KupacIme = ime;
            KupacPrezime = prezime;
            KupacKorisnickoIme = korisnickoIme;

            Deleted = deleted;

            Status = status;
            Tip = tip;
        }

        public override string ToString()
        {
            return $"{this.IdKarte},{this.Manifestacija.EventName},{this.Manifestacija.EventDateTime},{this.Datum},{this.Cena},{this.KupacKorisnickoIme},{this.Status.ToString()},{this.Tip.ToString()},{this.Deleted}";
        }
    }
}