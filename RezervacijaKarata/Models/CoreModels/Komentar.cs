﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaKarataProjekat.Models
{
    public enum Ocena {Nula, Jedan, Dva, Tri, Cetiri, Pet}
    public class Komentar
    {
        public Member Kupac { get; set; }
        public Manifestacija Manifestacija { get; set; }
        public String Tekst { get; set; }
        public Ocena Ocena { get; set; }
        public DateTime Posted { get; set; }
        public bool Approved { get; set; }
        public bool Deleted { get; set; }


        public Komentar(Member kupac, Manifestacija manifestacija, DateTime posted, string tekst, Ocena ocena, bool approved, bool deleted)
        {
            Kupac = kupac;
            Manifestacija = manifestacija;
            Tekst = tekst;
            Posted = posted;
            Ocena = ocena;
            Approved = approved;
            Deleted = deleted;
        }

        public override string ToString()
        {
            return $"{this.Kupac.Username},{this.Manifestacija.EventLocation},{this.Manifestacija.EventDateTime},{this.Posted},{this.Tekst},{this.Ocena.ToString()},{this.Approved},{this.Deleted}";
        }
    }
}