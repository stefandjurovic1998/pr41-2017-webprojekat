﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaKarataProjekat.Models
{
    public class Lokacija
    {
        public float GeoDuzina { get; set; }
        public float GeoSirina { get; set; }
        public Mesto Mesto { get; set; }

        public Lokacija(float geoDuzina, float geoSirina, Mesto mesto)
        {
            GeoDuzina = geoDuzina;
            GeoSirina = geoSirina;
            Mesto = mesto;
        }
    }
}