﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaKarataProjekat.Models
{
    public enum TipManifestacija {Concert, Festival, Play, Other}
    
    public class Manifestacija
    {
        public String EventName { get; set; }
        public TipManifestacija EventType { get; set; }
        public int MaximumAttendence { get; set; }

        public int TicketPrice { get; set; }
        public List<Tuple<string, int>> TicketType { get; set; }

        public DateTime EventDateTime { get; set; }
        
        public Mesto EventLocation { get; set; }
        public String EventPoster { get; set; }

        public bool LogicallyDeleted { get; set; }
        public bool ActivePost { get; set; }

        public int RegularTicketsSold { get; set; }
        public int FanPitTicketsSold { get; set; }
        public int VipTicketsSold { get; set; }

        public int TicketsSold { get; set; }

        public string CreatedBy { get; set; }

        public bool Finished { get; set; }

        public Manifestacija(string naziv, string createdBy, TipManifestacija tipManifestacija, int brojMesta, int regular, int fanpit, int vip, DateTime datumVremeOdrzavanja, int regularnaCenaKarte, Mesto mesto, string posterManifestacije, bool activated, bool deleted)
        {
            EventName = naziv;
            CreatedBy = createdBy;
            EventType = tipManifestacija;

            MaximumAttendence = brojMesta;

            EventDateTime = datumVremeOdrzavanja;
            TicketPrice = regularnaCenaKarte;
            EventLocation = mesto;
            EventPoster = posterManifestacije;

            RegularTicketsSold = regular;
            FanPitTicketsSold = fanpit;
            VipTicketsSold = vip;

            ActivePost = activated;
            LogicallyDeleted = deleted;
            TicketsSold = regular+fanpit+vip;
        }

        public override string ToString()
        {
            var ticketTypeString = $"{TicketType[0].Item2}/{TicketType[1].Item2}/{TicketType[2].Item2}";
            return $"{this.EventName},{this.CreatedBy},{this.EventType.ToString()},{this.MaximumAttendence},{this.RegularTicketsSold},{this.FanPitTicketsSold},{this.VipTicketsSold},{this.TicketPrice},{ticketTypeString},{this.EventDateTime.ToString()},{this.EventLocation.ToString()},{this.EventPoster},{this.ActivePost},{this.LogicallyDeleted}";
        }
    }
}