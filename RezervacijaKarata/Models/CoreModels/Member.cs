﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaKarataProjekat.Models
{
    public enum GenderEnum { Male, Female }
    public enum RoleEnum {Buyer, Seller, Admin }

    public class Member
    {
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        public GenderEnum Gender;
        public RoleEnum Role { get; set; }
        public int Points { get; set; }
        public Tip Membership { get; set; }
        public bool LogicallyDeleted { get; set; }

        /*
        public String FirstName { get; set; }
        public String LastName { get; set; }

        public DateTime BirthDate { get; set; }
        public GenderEnum Gender;

        public String Username { get; set; }
        public String Password { get; set; }

        // seller, buyer, admin
        public RoleEnum Role { get; set; }

        //public List<Karta> ReservedTickets { get; set; }
        //public List<Manifestacija> CreatedEvents { get; set; }
        public List<Object> MemberData { get; set; }
      
        public int Points { get; set; }
        public Tip Membership { get; set; }

        public bool LogicallyDeleted { get; set; }
        
        // new user registers
        public Member(string firstName, string lastName, string birthDate, string gender, string username, string password, string role, int points, bool deleted=false)
        {
            FirstName = firstName;
            LastName = lastName;

            BirthDate = DateTime.Parse(birthDate);
            Gender = (GenderEnum)(Enum.Parse(typeof(GenderEnum), gender));

            Username = username;
            Password = password;

            Points = points;
            Membership = new Tip(points);

            Role = (RoleEnum)Enum.Parse(typeof(RoleEnum), role);
            if (Role.Equals(RoleEnum.Buyer))
                MemberData = MemberData as List<Karta>;

            LogicallyDeleted = deleted;
        }

        public override string ToString()
        {
            return $"{this.FirstName},{this.LastName},{this.BirthDate.ToString("MM/dd/yyyy")},{this.Gender},{this.Username},{this.Password},{this.Role.ToString()},{this.Points},{this.LogicallyDeleted.ToString()}";
        }
        */
    }
}