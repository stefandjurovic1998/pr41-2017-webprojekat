﻿using RezervacijaKarataProjekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaKarata.Models.CoreModels.UserModels
{
    public class Buyer : Member
    {
        public List<String> ReservedTickets { get; set; }
        public bool Blocked { get; set; }
        public bool Suspicious { get; set; }
        public List<DateTime> ReservationActivity { get; set; }


        public Buyer(string firstName, string lastName, string birthDate, string gender, string username, string password, int points, bool deleted, bool blocked)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.BirthDate = DateTime.Parse(birthDate);
            this.Gender = (GenderEnum)Enum.Parse(typeof(GenderEnum), gender);
            this.Username = username;
            this.Password = password;
            this.Points = points;
            this.LogicallyDeleted = deleted;
            this.Role = RoleEnum.Buyer;
            this.ReservedTickets = new List<String>();
            this.Membership = new Tip(points);
            this.Blocked = blocked;
            this.ReservationActivity = new List<DateTime>();
            this.Suspicious = false;
        }

        public override string ToString()
        {
            return $"{this.FirstName},{this.LastName},{this.BirthDate.ToString("MM/dd/yyyy")},{this.Gender},{this.Username},{this.Password},{this.Points},{this.LogicallyDeleted.ToString()},{this.Blocked}";
        }
    }
}