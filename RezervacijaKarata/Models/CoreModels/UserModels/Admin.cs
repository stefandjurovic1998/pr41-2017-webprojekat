﻿using RezervacijaKarataProjekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaKarata.Models.CoreModels.UserModels
{
    public class Admin : Member
    {
        public Admin(string username, string password)
        {
            this.Username = username;
            this.Password = password;
            this.Role = RoleEnum.Admin;
        }

        public override string ToString()
        {
            return $"{this.Username},{this.Password}";
        }
    }
}