﻿using RezervacijaKarataProjekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaKarata.Models.CoreModels
{
    public class Seller : Member
    {
        public List<Manifestacija> CreatedEvents { get; set; }
        public bool Blocked;

        public Seller(string firstName, string lastName, string birthDate, string gender, string username, string password, int points, bool deleted, bool blocked)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.BirthDate = DateTime.Parse(birthDate);
            this.Gender = (GenderEnum)Enum.Parse(typeof(GenderEnum), gender);
            this.Username = username;
            this.Password = password;
            this.Points = points;
            this.LogicallyDeleted = deleted;
            this.Role = RoleEnum.Seller;
            this.CreatedEvents = new List<Manifestacija>();
            this.Membership = new Tip(points);
            this.Blocked = blocked;
        }

        public override string ToString()
        {
            return $"{this.FirstName},{this.LastName},{this.BirthDate.ToString("MM/dd/yyyy")},{this.Gender},{this.Username},{this.Password},{this.Points},{this.LogicallyDeleted.ToString()},{this.Blocked}";
        }
    }
}