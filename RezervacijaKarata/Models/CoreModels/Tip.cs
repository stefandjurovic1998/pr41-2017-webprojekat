﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaKarataProjekat.Models
{
    public enum TipKorisnikaEnum {Zlatni, Srebrni, Bronzani}
    public class Tip
    {
        public Tip(TipKorisnikaEnum tipKorisnika, double popust, int bodovi)
        {
            TipKorisnika = tipKorisnika;
            Popust = popust;
            Bodovi = bodovi;
        }

        public Tip(int bodovi, string tip="Bronzani")
        {
            TipKorisnika = (TipKorisnikaEnum)(Enum.Parse(typeof(TipKorisnikaEnum), tip));

            if (bodovi <= 3000)
            {
                Popust = 1.0;
            }
            else if (bodovi <= 6000)
            {
                Popust = 0.95;
            }
            else
            {
                Popust = 0.90;
            }

            Bodovi = bodovi;
        }

        public Tip(int bodovi)
        {
            if (bodovi <= 3000)
            {
                TipKorisnika = TipKorisnikaEnum.Bronzani;
                Popust = 1;
            }
            else if (bodovi <= 6000)
            {
                TipKorisnika = TipKorisnikaEnum.Srebrni;
                Popust = 0.95;
            }
            else
            {
                TipKorisnika = TipKorisnikaEnum.Zlatni;
                Popust = 0.90;
            }
            Bodovi = bodovi;
        }

        public TipKorisnikaEnum TipKorisnika { get; set; }
        public double Popust { get; set; }
        public int Bodovi { get; set; }
    }
}