﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaKarataProjekat.Models
{
    public class Mesto
    {
        public Mesto(string ulicaIBroj, string grad, string postanskiBroj)
        {
            this.UlicaIBroj = ulicaIBroj;
            this.Grad = grad;
            this.PostanskiBroj = postanskiBroj;
        }

        public Mesto(string ulicaIBroj, string grad, string postanskiBroj, string lat, string lng)
        {
            this.UlicaIBroj = ulicaIBroj;
            this.Grad = grad;
            this.PostanskiBroj = postanskiBroj;
            this.Longitude = lng;
            this.Latitude = lat;
        }

        public String UlicaIBroj { get; set; }
        public String Grad { get; set; }
        public String PostanskiBroj { get; set; }

        public String Longitude { get; set; }
        public String Latitude { get; set; }

        public override string ToString()
        {
            return String.Format("{0},{1},{2},{3},{4}", UlicaIBroj, Grad, PostanskiBroj, Latitude, Longitude);
        }
    }
}