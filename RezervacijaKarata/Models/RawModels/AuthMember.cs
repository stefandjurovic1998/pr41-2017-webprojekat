﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaKarata.Models
{
    public class AuthMember
    {
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        public String BirthDate { get; set; }
        public String Gender { get; set; }

        public String MemberType { get; set; }

        public AuthMember(string firstName, string lastName, string username, string password, string birthDate, string gender)
        {
            FirstName = firstName;
            LastName = lastName;
            Username = username;
            Password = password;
            BirthDate = birthDate;
            Gender = gender;
        }

        public override string ToString()
        {
            return $"{this.FirstName},{this.LastName},{this.BirthDate},{this.Gender},{this.Username},{this.Password}";
        }
    }
}