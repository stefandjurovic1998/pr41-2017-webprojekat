﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaKarata.Models
{
    public class RawEvent
    {
        public RawEvent(string eventName, string eventType, string eventDate, string ticketPrice, string maximumAttendence, string regularTickets, string vipTickets, string fanPitTickets, string eventStreetAdress, string eventCity, string eventPostalCode, string eventPoster)
        {
            EventName = eventName;
            EventType = eventType;
            EventDate = eventDate;
            TicketPrice = ticketPrice;
            MaximumAttendence = maximumAttendence;
            RegularTickets = regularTickets;
            VipTickets = vipTickets;
            FanPitTickets = fanPitTickets;
            EventStreetAdress = eventStreetAdress;
            EventCity = eventCity;
            EventPostalCode = eventPostalCode;
            EventPoster = eventPoster;
        }

        public String EventName { get; set; }
        public String EventType { get; set; }
        public String EventDate { get; set; }

        public String TicketPrice { get; set; }

        public String MaximumAttendence { get; set; }

        public String RegularTickets { get; set; }
        public String VipTickets { get; set; }
        public String FanPitTickets { get; set; }

        public String EventStreetAdress { get; set; }
        public String EventCity { get; set; }
        public String EventPostalCode { get; set; }

        public String EventPoster { get; set; }

        public String CurrentEventName { get; set; }
        public String CurrentDate { get; set; }
        public String CurrentLocation { get; set; }
        public String CurrentCity{ get; set; }
        public String CurrentPostalCode { get; set; }

        public String EventLatitude { get; set; }
        public String EventLongitude { get; set; }

        public String CreatedBy { get; set; }
    }
}