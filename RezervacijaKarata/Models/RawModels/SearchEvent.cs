﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaKarata.Models
{
    public class SearchEvent
    {
        public string EventName { get; set; }
        public string EventLocation { get; set; }
        public string EventCity { get; set; }

        public string DateFrom { get; set; }
        public string DateTo { get; set; }

        public string PriceFrom { get; set; }
        public string PriceTo { get; set; }

        public SearchEvent(string eventName, string eventLocation, string eventCity, string dateFrom, string dateTo, string priceFrom, string priceTo)
        {
            EventName = eventName;
            EventLocation = eventLocation;
            EventCity = eventCity;
            DateFrom = dateFrom;
            DateTo = dateTo;
            PriceFrom = priceFrom;
            PriceTo = priceTo;
        }
    }
}