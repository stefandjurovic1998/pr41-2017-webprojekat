﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaKarata.Models.RawModels
{
    public class RawComment
    {
        public RawComment(string commentText, string commentRating)
        {
            CommentText = commentText;
            CommentRating = commentRating;
        }

        public String CommentText { get; set; }
        public String CommentRating { get; set; }

        public String EventName { get; set; }
        public String EventDate { get; set; }

        public String EventStreetAdress { get; set; }
        public String EventCity { get; set; }
        public String EventPostalCode { get; set; }

        public String PostedWhen { get; set; }
        public String PostedBy { get; set; }



    }
}