﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaKarata.Models
{
    public class RawTicket
    {
        public RawTicket(string ticketId, string eventName, string date, string price, string memberUsername, string status, string tip)
        {
            TicketId = ticketId;
            EventName = eventName;
            Date = date;
            Price = price;
            MemberUsername = memberUsername;
            Status = status;
            Tip = tip;
        }

        public string TicketId { get; set; }
        public string EventName { get; set; }
        public string Date { get; set; }
        public string Price { get; set; }
        public string MemberUsername { get; set; }
        public string Status { get; set; }
        public string Tip { get; set; }
    }
}