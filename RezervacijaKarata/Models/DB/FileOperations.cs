﻿using RezervacijaKarata.Models.CoreModels;
using RezervacijaKarata.Models.CoreModels.UserModels;
using RezervacijaKarataProjekat.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace RezervacijaKarata.Models
{   
    //  main class for working with files, reading and updating values from them
    public class FileOperations
    {   
        // appends newly added data to the passed file
        public void UpdateFile(string filename, string content)
        {
            List<Member> members = new List<Member>();
            filename = HostingEnvironment.MapPath(filename);
            FileStream fileStream = new FileStream(filename, FileMode.Append);
            StreamWriter streamWriter = new StreamWriter(fileStream);
            streamWriter.Write(content);
        }

        // reads data for the passed filename
        // $"{this.FirstName},{this.LastName},{this.BirthDate},{this.Gender},{this.Username},{this.Password},{this.Role},{this.Membership},{this.Points}";
        public List<Buyer> ReadBuyersFile(string filename)
        {
            var loadedMembers = new List<Buyer>();

            var fileContents = File.ReadAllText(HostingEnvironment.MapPath(@"~/App_Data/"+filename));
            var loadedMembersString = fileContents.Split(new[] { "\n" }, StringSplitOptions.None);

            foreach(var loadedString in loadedMembersString)
            {
                if(!string.IsNullOrWhiteSpace(loadedString))
                {
                    var splitString = loadedString.Split(new[] { "," }, StringSplitOptions.None);

                    // new look of the Member Constructor
                    // Member(string firstName, string lastName, string birthDate, string gender, string username, string password, string role, int points)
                    Buyer newMember = new Buyer(splitString[0], splitString[1], splitString[2], splitString[3], splitString[4], splitString[5], int.Parse(splitString[6]), bool.Parse(splitString[7]), bool.Parse(splitString[8]));
                    loadedMembers.Add(newMember);
                }
            }
            return loadedMembers;
        }

        public List<Seller> ReadSellersFile(string filename)
        {
            var loadedMembers = new List<Seller>();

            var fileContents = File.ReadAllText(HostingEnvironment.MapPath(@"~/App_Data/" + filename));
            var loadedMembersString = fileContents.Split(new[] { "\n" }, StringSplitOptions.None);

            foreach (var loadedString in loadedMembersString)
            {
                if (!string.IsNullOrWhiteSpace(loadedString))
                {
                    var splitString = loadedString.Split(new[] { "," }, StringSplitOptions.None);
                    Seller newMember = new Seller(splitString[0], splitString[1], splitString[2], splitString[3], splitString[4], splitString[5], int.Parse(splitString[6]), bool.Parse(splitString[7]),bool.Parse(splitString[8]));
                    loadedMembers.Add(newMember);
                }
            }
            return loadedMembers;
        }

        public List<Admin> ReadAdminsFile(string filename)
        {
            var loadedMembers = new List<Admin>();
            var fileContents = File.ReadAllText(HostingEnvironment.MapPath(@"~/App_Data/" + filename));
            var loadedMembersString = fileContents.Split(new[] { "\n" }, StringSplitOptions.None);

            foreach (var loadedString in loadedMembersString)
            {
                if (!string.IsNullOrWhiteSpace(loadedString))
                {
                    var splitString = loadedString.Split(new[] { "," }, StringSplitOptions.None);
                    Admin newMember = new Admin(splitString[0], splitString[1]);
                    loadedMembers.Add(newMember);
                }
            }
            return loadedMembers;
        }

        public List<Manifestacija> ReadEventFile(string filename)
        {
            var loadedEvents = new List<Manifestacija>();
            var fileContents = File.ReadAllText(HostingEnvironment.MapPath(@"~/App_Data/" + filename));
            var loadedEventStrings = fileContents.Split(new[] { "\n" }, StringSplitOptions.None);

            foreach(var loadedString in loadedEventStrings)
            {
                if(!string.IsNullOrWhiteSpace(loadedString))
                {
                    var splitString = loadedString.Split(new[] { "," }, StringSplitOptions.None);
                    //Exit,ew0ny,Festival,5000,0,6000,4000/500/500,9/17/2020 10:55:00 PM,Petrovaradinska Tvrdjava,Novi Sad,21000,C:\fakepath\14572869_10154530177168698_7285320179593035491_n.png,true,False
                    var typeEnum = (TipManifestacija)Enum.Parse(typeof(TipManifestacija), splitString[2]);
                    var eventLocation = new Mesto(splitString[10], splitString[11], splitString[12], splitString[13], splitString[14]);
                    var ticketTypePrices = splitString[8].Split(new[] { "/" }, StringSplitOptions.None);
                    //string naziv, string createdBy, TipManifestacija tipManifestacija, int brojMesta, int ticketsSold, DateTime datumVremeOdrzavanja, int regularnaCenaKarte, Mesto mesto, string posterManifestacije, bool activated, bool deleted

                    Manifestacija newEvent = new Manifestacija(splitString[0], splitString[1], typeEnum, int.Parse(splitString[3]), int.Parse(splitString[4]), int.Parse(splitString[5]), int.Parse(splitString[6]), DateTime.Parse(splitString[9]), int.Parse(splitString[7]), eventLocation, splitString[15], bool.Parse(splitString[16]), bool.Parse(splitString[17]));
                    newEvent.TicketType = new List<Tuple<string, int>>(3)
                    {
                        Tuple.Create("Regular", int.Parse(ticketTypePrices[0])),
                        Tuple.Create("FanPit", int.Parse(ticketTypePrices[1])),
                        Tuple.Create("Vip", int.Parse(ticketTypePrices[2])),
                    };
                    loadedEvents.Add(newEvent);
                }
            }
            return loadedEvents;
        }

        public List<Karta> ReadTicketsFile(string filename)
        {
            var loadedTickets= new List<Karta>();
            var fileContents = File.ReadAllText(HostingEnvironment.MapPath(@"~/App_Data/" + filename));
            var loadedTicketStrings = fileContents.Split(new[] { "\n" }, StringSplitOptions.None);

            foreach (var loadedString in loadedTicketStrings)
            {
                if (!string.IsNullOrWhiteSpace(loadedString))
                {
                    var splitString = loadedString.Split(new[] { "," }, StringSplitOptions.None);
                    var foundEvent = EventDB.eventDB.Find(evnt => ((evnt.EventName.Equals(splitString[1])) && (evnt.EventDateTime.Equals(DateTime.Parse(splitString[2])))));
                    var foundMember = MembersDB.buyersDB.Find(mem => (mem.Username.Equals(splitString[5])));

                    var statusType = (StatusKarte)Enum.Parse(typeof(StatusKarte), splitString[6]);
                    var tipType = (TipKarte)Enum.Parse(typeof(TipKarte), splitString[7]);

                    //6ZT0ZPK079,Sea Dance sa Slikom,7/28/2020 10:00:00 PM,7/28/2020 10:00:00 PM,4275,dzoni,Rezervisana,REGULAR,False
                    Karta newTicket = new Karta(splitString[0], foundEvent, DateTime.Parse(splitString[3]), double.Parse(splitString[4]), foundMember.FirstName, foundMember.LastName, foundMember.Username, statusType, tipType, bool.Parse(splitString[8]));
                    loadedTickets.Add(newTicket);
                }
            }
            return loadedTickets;
        }

        public List<Komentar> ReadCommentsFile(string filename)
        {
            var loadedComments = new List<Komentar>();
            var fileContents = File.ReadAllText(HostingEnvironment.MapPath(@"~/App_Data/" + filename));
            var loadedCommentsStrings = fileContents.Split(new[] { "\n" }, StringSplitOptions.None);

            foreach (var loadedString in loadedCommentsStrings)
            {
                if (!string.IsNullOrWhiteSpace(loadedString))
                {
                    //dzoni,Plaza Jaz,Budva,85310,40.979898069620155,-101.95312500000001,7/28/2020 10:00:00 PM,9/20/2020 1:53:15 PM,odlican festival  vraticu se sigurno,Pet,False,False

                    var splitString = loadedString.Split(new[] { "," }, StringSplitOptions.None);
                    var foundEvent = EventDB.eventDB.Find(evnt => (evnt.EventLocation.UlicaIBroj.Equals(splitString[1])) &&
                                                          (evnt.EventLocation.Grad.Equals(splitString[2])) && (evnt.EventLocation.PostanskiBroj.Equals(splitString[3]) &&
                                                          (evnt.EventDateTime.Equals(DateTime.Parse(splitString[6])))));
                    var foundMember = MembersDB.buyersDB.Find(mem => mem.Username.Equals(splitString[0]));
                    Komentar newComment = new Komentar(foundMember, foundEvent, DateTime.Parse(splitString[7]), splitString[8], (Ocena)Enum.Parse(typeof(Ocena), splitString[9]), bool.Parse(splitString[10]), bool.Parse(splitString[11]));
                    loadedComments.Add(newComment);
                }
            }
            return loadedComments;
        }

        // overwrites all of the data in the passed file with new data
        public void SaveObject(string filename, string objectContent)
        {
            using (StreamWriter stream = new FileInfo(HostingEnvironment.MapPath(@"~/App_Data/" + filename)).AppendText())
            {
                stream.WriteLine(objectContent);
            }
        }

        public void SaveFile(string filename, IEnumerable<object> outputStructure)
        {
            using (StreamWriter stream = new FileInfo(HostingEnvironment.MapPath(@"~/App_Data/" + filename)).CreateText())
            {
                foreach(var obj in outputStructure)
                    stream.WriteLine(obj.ToString());
            }
        }
    }
}